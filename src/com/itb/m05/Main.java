package com.itb.m05;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int number1;
        int number2;
        String operation;
        float result = 0;

        System.out.println("Quina operació vols fer? [multiplicar, dividir]");
        operation = sc.nextLine();
        System.out.print("Introdueix el primer numero: ");
        number1 = sc.nextInt();
        System.out.print("Introdueix el segon numero: ");
        number2 = sc.nextInt();

        switch(operation)
        {
            case "multiplicar":
                result = number1 * number2;
                break;
            case "dividir":
                result = (float) number1 / (float) number2;
                break;
            default:
                System.out.println("Operació no disponible");
        }

        System.out.println("Resultat: "+result);
    }
}
